'''
Created on Apr 16, 2014

@author: nathan

The various track segments

TODO: rewrite to make smarter
'''


from abc import ABCMeta, abstractmethod


class SegmentBase(metaclass=ABCMeta):
    __slots__ = ('length', 'switch', 'parent', 'base_length')

    def __init__(self, parent, length, switch):
        self.parent = parent
        self.length = length
        self.switch = switch

        # base_length is the length of the track leading up to this segment
        if self.parent is not None:
            self.base_length = parent.length + parent.base_length
        else:
            self.base_length = 0

    @abstractmethod
    def location_at(self, ratio):
        '''
        Return position, angle
        '''
        raise NotImplementedError

    def location_at_distance(self, distance):
        return self.location_at(distance / self.length)


class StraightSegment(SegmentBase):
    __slots__ = ('length')

    def __init__(self, parent, length, switch):
        SegmentBase.__init__(self, parent, length, switch)
        self.length = length

    def location_at(self, ratio):
        raise NotImplementedError


class CurveSegment(SegmentBase):
    __slots__ = ('angle', 'radius')

    def __init__(self, parent, angle, radius, switch):
        SegmentBase.__init__(self, parent, angle * radius, switch)
        self.angle = angle
        self.radius = radius

    def location_at(self, ratio):
        raise NotImplementedError


def make_segment(parent, data):
    '''
    Create a Segment object based on a JSON dict
    '''
    try:
        return StraightSegment(
            parent,
            data['length'],
            data.get('switch', False))
    except KeyError:
        pass

    try:
        return CurveSegment(
            parent,
            data['angle'],
            data['radius'],
            data.get('switch', False))
    except KeyError:
        pass

    raise ValueError('Unable to create segment from JSON', data)


def make_all_segments(data):
    '''
    Create all the segments in the list from the JSON. Updates the parents
    correctly.
    '''
    segment = None
    for segment_data in data:
        segment = make_segment(segment, segment_data)
        yield segment
