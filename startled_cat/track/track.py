'''
Created on Apr 17, 2014

@author: nathan
'''

from startled_cat.track.segments import make_all_segments


class Track:
    def __init__(self, track_data):
        '''
        Initialize the track. track_data should be the JSON dict recieved from
        the server
        '''
        self.id = track_data['id']
        self.name = track_data['name']
        self.segments = list(make_all_segments(track_data['pieces']))
        self.lanes = [lane['distanceFromCenter']
                      for lane in track_data['lanes']]
