'''
Created on Apr 15, 2014

@author: nathan

Library for registering handlers for the recieved json objects
'''


class MethodLookupError(KeyError):
    pass


class MethodRegistry:
    def __init__(self):
        self.handlers = {}

    def register(self, message_type):
        def decorator(func):
            self.handlers[message_type] = func
            return func
        return decorator

    def __get__(self, instance, owner):
        '''
        When accessed as an instance object, create a callable that binds a
        handler to self. For example:

        class Foo:
            handlers = MethodRegistry()

            def thing(self, thing):
                return thing

            @handlers.register('hello')
            def func(self, data):
                return self.thing(data)

            def foo(self):
                message = {'msgType': "hello", 'data': "world"}
                assert self.handlers(message) == "world"

        '''
        if instance is None:
            return self

        def run_handler(msgType, data):
            try:
                cls_handler = self.handlers[msgType]
            except KeyError:
                raise MethodLookupError(msgType)
            else:
                return cls_handler(instance, data)
        return run_handler
