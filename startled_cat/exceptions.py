'''
Created on Apr 18, 2014

@author: nathan

Standard exceptions used as a part of the bot code
'''


class FlowError(RuntimeError):
    '''
    Raise when an unexpected message is recieved and the error is unrecoverable
    '''
    pass


class GameEnd(RuntimeError):
    '''
    Raise on a game end. Caught by the runner.
    '''
    pass