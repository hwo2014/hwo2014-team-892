'''
Created on Apr 18, 2014

@author: nathan

Various helpers for creating bots. Bot classes should be derived from BaseBot.
To add a bot class or function to the global all_bots dict, decorate it with
register_bot. The bot function or bot class should either return the gameEnd
data when that message is received, or use the bot_read_msg function so that
an exception is raised automatically, allowing for transparent detection of
gameEnd.
'''

from abc import ABCMeta, abstractmethod
import asyncio
from startled_cat.comm import read_msg
from startled_cat.exceptions import GameEnd

all_bots = {}


def register_bot(name, *, default=False):
    '''
    Register a bot class or bot function to the global bot dict
    '''
    def decorator(Bot):
        # If this bot is a class, create a wrapper function
        if isinstance(Bot, type):
            @asyncio.coroutine
            def bot_process(reader, writer, track, name, color):
                return Bot(reader, writer, track, name, color).run()
        else:
            bot_process = Bot

        all_bots[name] = bot_process
        if default:
            all_bots[None] = bot_process
        return Bot
    return decorator


class BaseBot(metaclass=ABCMeta):
    '''
    Base class for bots. Defines a standard format constructor that stores
    the necessary attributes. Sublasses should override `run`, and have it
    return the gameEnd data at the end of the race
    '''
    def __init__(self, reader, writer, track, name, color):
        self.reader = reader
        self.writer = writer
        self.track = track
        self.name = name
        self.color = color

    @abstractmethod
    def run(self):
        raise NotImplementedError


def get_self(name, data):
    '''
    Get a specific car from a 'carPositions' message
    '''
    return next(car for car in data if car['id']['name'] == name)


@asyncio.coroutine
def bot_read_msg(reader):
    '''
    Functions just like read_msg, except that it raises a GameEnd on a gameEnd
    message. This exception is caught by the runner. The idea is that bots can
    use this function and not have to worry about gameEnds.
    '''
    msgType, data = yield from read_msg(reader)
    if msgType == 'gameEnd':
        raise GameEnd(data)
    return msgType, data
