'''
Created on Apr 18, 2014

@author: nathan

The noob bot, supplied by the skeleton code. Behaviorly identical, but updated
to use cool stuff like asyncio and registry
'''

import asyncio
from startled_cat import registry, botlib
from startled_cat.comm import read_msg, throttle, ping


@botlib.register_bot('NoobBot', default=True)
class NoobBot(botlib.BaseBot):
    handlers = registry.MethodRegistry()

    @handlers.register("carPositions")
    def on_car_positions(self, data):
        return throttle(self.writer, 0.5)

    @handlers.register("crash")
    def on_crash(self, data):
        print("Someone crashed")
        return ping(self.writer)

    @handlers.register("error")
    def on_error(self, data):
        print("Error: {0}".format(data))
        return ping(self.writer)

    @asyncio.coroutine
    def run(self):
        while True:
            msgType, data = yield from botlib.bot_read_msg(self.reader)
            try:
                yield from self.handlers(msgType, data)
            except registry.MethodLookupError:
                print("Unrecognized message type:", msgType)
                yield from ping(self.writer)
