'''
This package contains all bot implementations. The files should be imported
here, to trigger the registration.
'''

from . import noob_bot, nathan_bot
