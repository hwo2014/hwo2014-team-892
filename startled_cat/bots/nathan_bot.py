'''
Created on Apr 21, 2014

@author: nathan
'''

from startled_cat.botlib import register_bot, get_self, bot_read_msg
from startled_cat.comm import ping, switch_lane, throttle
from startled_cat.track.segments import CurveSegment, StraightSegment


@register_bot("nathan-bot")
def nathan_bot(reader, writer, track, name, color):
    while True:
        msgType, data = yield from bot_read_msg(reader)
        if msgType != 'carPositions':
            print("Recieved", msgType)
            ping(writer)

        else:
            self = get_self(name, data)

            current_segment = track.segments[
                self['piecePosition']['pieceIndex']]

            if isinstance(current_segment, CurveSegment):
                throttle(writer, 0.3)
            else:
                throttle(writer, 0.9)

