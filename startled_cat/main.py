'''
MAIN! This is the file to execute to run the bot. Handles parsing command line
arguments and launching the runner
'''


import argparse
import asyncio

from startled_cat import config, bots, botlib, runner


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-b", "--bot")
    parser.add_argument("host", nargs="?", default=config.default_host)
    parser.add_argument("port", type=int, nargs="?",
        default=config.default_port)
    parser.add_argument("name", nargs="?", default=config.bot_name)
    parser.add_argument("key", nargs="?", default=config.bot_key)

    args = parser.parse_args()

    print("Connecting with parameters:\n"
        "Host: {args.host}\n"
        "Port: {args.port}\n"
        "Bot Name: {args.name}\n"
        "Key: {args.key}".format(args=args))

    bot = botlib.all_bots[args.bot]

    loop = asyncio.get_event_loop()
    loop.run_until_complete(runner.run_basic(
        bot, args.name, args.key, args.host, args.port))

if __name__ == "__main__":
    main()
