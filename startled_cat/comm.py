'''
Created on Apr 17, 2014

@author: nathan

Communication utilities
'''

import asyncio
from json import dumps, loads

from startled_cat.exceptions import FlowError


#====================================================================
# Writing utilities
#====================================================================
def send_msg(writer, msg_type, data):
    '''
    Send a message to the server. This function is a coroutine.
    '''
    writer.writelines((dumps({"msgType": msg_type, "data": data},
    ensure_ascii=False, check_circular=False, separators=(',', ':')
    ).encode('utf8'), b'\n'))
    return writer.drain()


def throttle(writer, amount):
    '''
    Send a throttle message to the server
    '''
    return send_msg(writer, "throttle", amount)


def ping(writer):
    '''
    Send a ping to the server
    '''
    return send_msg(writer, "ping", {})


def switch_lane(writer, direction):
    '''
    Send a switch lane request to the server
    '''
    return send_msg(writer, "switchLane", direction)


def join(writer, name, key):
    '''
    Join a new game on the server
    '''
    return send_msg(writer, 'join', {'name': name, 'key': key})


#====================================================================
# Reading utilites
#====================================================================

@asyncio.coroutine
def read_msg(reader, *valid_types):
    '''
    Read a single message from the server. Raise an exception if the message
    type doesn't match one of the given types.
    '''
    # Read raw message from server
    msg_bytes = yield from reader.readline()

    # Check for disconnects
    if not msg_bytes.endswith(b'\n'):
        raise RuntimeError('Unexpected Disconnect from server')

    # Convert JSON to dict
    msg = loads(msg_bytes.decode('utf8'))
    msgType, data = msg['msgType'], msg['data']

    # Check for valid message type
    if valid_types and msgType not in valid_types:
        raise FlowError('Unexpected message type', msgType)

    # Return
    return msgType, data
