'''
Created on Apr 18, 2014

@author: nathan

This module contains function(s) for bootstrapping a race and launching a bot.
'''

import asyncio

from startled_cat import comm
from startled_cat.exceptions import GameEnd
from startled_cat.track.track import Track


@asyncio.coroutine
def run_basic(Bot, name, key, host, port):
    '''
    Basic run. Connect to the server, load the track, then launch one bot for
    each race that is run.
    '''
    reader, writer = yield from asyncio.open_connection(host, port)
    print("Connected to {}:{}".format(host, port))

    yield from comm.join(writer, name, key)

    # Server responds with join message
    yield from comm.read_msg(reader, 'join')
    print("Joined server")

    # Server sends yourCar
    _, data = yield from comm.read_msg(reader, 'yourCar')
    color = data['color']
    print("Joined game as {} car".format(color))

    # Server sends gameInit
    _, data = yield from comm.read_msg(reader, 'gameInit')
    print("Recieved gameInit data")

    track = Track(data['race']['track'])
    game_results = []

    # NOT PART OF THE TECH SPEC: Server sends initial carPositions
    yield from comm.read_msg(reader, 'carPositions')

    # Aparently all carPositons, even the first, require a ping response
    yield from comm.ping(writer)

    # Server sends gameStart
    yield from comm.read_msg(reader, 'gameStart')

    while True:
        print("Beginning race!")

        try:
            gameEnd = yield from Bot(reader, writer, track, name, color)
        except GameEnd as e:
            gameEnd = e.args[0]

        print("Race finished!")
        game_results.append(gameEnd)
        msgType, data = yield from comm.read_msg(
            reader, 'gameInit', 'tournamentEnd')
        if msgType == 'tournamentEnd':
            print("All races complete")
            return game_results
