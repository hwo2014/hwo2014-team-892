'''
Created on Apr 16, 2014

@author: nathan
'''


import collections
from math import sqrt, atan2, sin, cos


class Vec2D(collections.namedtuple('Vec2D', ('x', 'y'))):
    '''
    2D vector with math operations
    '''
    @classmethod
    def zero(cls):
        return cls(0, 0)

    @classmethod
    def unit_angle(cls, angle):
        return Vec2D(cos(angle), sin(angle))

    def __add__(self, other):
        return Vec2D(self[0] + other[0], self[1] + other[1])

    def __sub__(self, other):
        return Vec2D(self[0] - other[0], self[1] - other[1])

    def __mul__(self, value):
        return Vec2D(self[0] * value, self[1] * value)

    def __div__(self, value):
        return Vec2D(self[0] / value, self[1] / value)

    @property
    def magnitude_squared(self):
        return self[0] ** 2 + self[1] ** 2

    @property
    def magnitude(self):
        return sqrt(self.magnitude_squared)

    @property
    def angle(self):
        return atan2(self[1], self[0])

    def normalized(self):
        return self / self.magnitude

    def to_length(self, length):
        return self * (length / self.magnitude)

    def to_angle(self, angle):
        return Vec2D(cos(angle), sin(angle)) * self.magnitude

    def rotate(self, angle):
        return self if angle == 0 else self.to_angle(self.angle + angle)
